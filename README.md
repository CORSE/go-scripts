## Requierements
- perl. 
- Sur linux il faut locate (package mlocate). Sous macos ça utilise mdfind. 

## Installation:
1. Placer go.pl et gopen.pl dans le PATH
1. Rajouter les lignes suivantes dans son .bashrc
```bash
alias open='xdg-open' # Sur linux seulement
function go() { go.pl $*;  cd `cat /tmp/goto`; } 
function gopen() { gopen.pl $*; GPATH=`cat /tmp/goto`; echo "moving to $GPATH"; cd $GPATH; if test -f "/tmp/open"; then GFILE=`cat /tmp/open`; echo "opening $GFILE"; open "$GFILE"; fi }
```
1. Pour mac c'est possible que la méthode pour vérifier que l'on est sous macos ne fonctionne plus (j'ai vu un darwin trainer dans le script).

## Usage de go
- dans chaque répertoire, éditer un fichier `keywords` qui contient une liste de mots clés séparés par des virgules
- `go` va reconstruire la "base de données" 
- `go mot_cle` va proposer une liste de répertoire qui matchent la requête. Si un seul match, ça va faire un `cd`. Sinon, 0 c'est le dernier accédé, puis dans l'ordre décroissant de date d'accès. [ENTER] est équivalent à [0].

## Usage de gopen
- Pour chaque fichier de nom `nom_fichier.ext`, éditer un fichier de nom `nom_fichier.ext.Kw` avec les mots clés correspondants
- `gopen` reconstruit la base de données
- `gopen mot_clé` fait comme `go mot_clé` mais avec ouverture du fichier correspondant en plus de `cd` dans le répertoire.
